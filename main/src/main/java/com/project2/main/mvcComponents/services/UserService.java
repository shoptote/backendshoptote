package com.project2.main.mvcComponents.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

import com.project2.main.mvcComponents.models.Users;
import com.project2.main.mvcComponents.repositories.UsersRepository;



@Service
public class UserService {

	@Autowired
	private UsersRepository ur;

	public boolean userRegistration(Users users) {
		Users exists = ur.findByUsername(users.getUsername());
		//Inelegant, may cause phantom reads.
		if(exists != null){
			return false;
		}
		ur.save(users);
		return true;

	}

	public Users getUserByUsername(String name){

		Users exists = ur.findByUsername(name);
		return exists;
	}
}
