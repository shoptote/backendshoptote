package com.project2.main.mvcComponents.repositories;

import java.util.List;

import com.project2.main.mvcComponents.models.RecipeItems;

import org.springframework.data.repository.CrudRepository;

public interface RecipeItemsRepository extends CrudRepository<RecipeItems, Integer>{
    
    List<RecipeItems> findByRecipeId(Integer recipeId);
}