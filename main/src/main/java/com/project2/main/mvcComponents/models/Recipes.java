package com.project2.main.mvcComponents.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name ="recipes")
public class Recipes {
    

    @Id
    @NotNull(message="Please enter item id")
    @Column(name="recipeId")
    @Min(value=1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int recipeId;

    @Column(name="recipeName", unique=true)
    @NotEmpty(message="Please enter recipe name")
    private String name;

    @Column(name ="apiId")
    private int apiId;
}