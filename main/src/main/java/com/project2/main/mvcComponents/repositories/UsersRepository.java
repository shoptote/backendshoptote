package com.project2.main.mvcComponents.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project2.main.mvcComponents.models.Users;



public interface UsersRepository extends JpaRepository<Users, Integer>{
    Users findByUsername(String username);
}
