package com.project2.main.mvcComponents.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project2.main.mvcComponents.models.FavoriteRecipes;
import com.project2.main.mvcComponents.models.ShoppingItems;
import com.project2.main.mvcComponents.services.FavoriteRecipesService;


@RequestMapping("/favorite")
@RestController
public class FavoriteRecipesController {

	@Autowired
	private FavoriteRecipesService fs;
	
	@PostMapping("/add")
	public FavoriteRecipes addFavorite(@RequestBody FavoriteRecipes recipes) {
		fs.addFavorite(recipes);
		return recipes;
	}

	@GetMapping("/all")
	public List<FavoriteRecipes> getAllFavorite(){
		return fs.findAll();
	}
	
	@DeleteMapping("/deletebyid/{id}")
	public void deleteById(@PathVariable Integer id) {
		fs.deleteById(id);;
	}
	
	
}
