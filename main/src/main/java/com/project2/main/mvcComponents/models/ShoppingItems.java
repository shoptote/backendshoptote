package com.project2.main.mvcComponents.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shopping_items")
public class ShoppingItems implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ Column(name="shopping_items_id")
	private int id;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="ingredient_id")
	private int ingredientId;
	
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="unit")
	private String unit;
	
	@Column(name="bought")
	private boolean bought;
	
	public ShoppingItems () {	
	}

	public ShoppingItems(int id, int user_id, int ingredient_id, int quantity, String unit, boolean bought) {
		super();
		this.id = id;
		this.userId = user_id;
		this.ingredientId = ingredient_id;
		this.quantity = quantity;
		this.unit = unit;
		this.bought = bought;
	}

	public ShoppingItems(int user_id, int ingredient_id, int quantity, String unit, boolean bought) {
		super();
		this.userId = user_id;
		this.ingredientId = ingredient_id;
		this.quantity = quantity;
		this.unit = unit;
		this.bought = bought;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return userId;
	}

	public void setUser_id(int user_id) {
		this.userId = user_id;
	}

	public int getIngredient_id() {
		return ingredientId;
	}

	public void setIngredient_id(int ingredient_id) {
		this.ingredientId = ingredient_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}
	
	
	
}
