package com.project2.main.mvcComponents.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.project2.main.mvcComponents.models.Ingredients;
import com.project2.main.mvcComponents.repositories.IngredientsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IngredientsService {
    @Autowired
    private IngredientsRepository ingredientsRepository;

    private List<Ingredients> ingredients = new ArrayList<Ingredients>();

    public List<Ingredients> getAllIngredients(){
        List<Ingredients> ingredients = new ArrayList<>();
        ingredientsRepository.findAll().forEach(ingredients::add);
        return ingredients;
    }

    public void addIngredient(Ingredients ingredient){
        if(ingredientsRepository.findByName(ingredient.getName()) != null){return;}
        ingredientsRepository.save(ingredient);
    }

    public Ingredients getIngredientId(String name){
        return ingredientsRepository.findByName(name);
    }

    public Optional<Ingredients> findByIngredientId(Integer id){
        return ingredientsRepository.findById(id);
    }

    public List<Ingredients> findByIngredientsLike(String name){
        return ingredientsRepository.findByNameLike(name);
    }

}
