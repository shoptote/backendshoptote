package com.project2.main.mvcComponents.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.project2.main.mvcComponents.models.ShoppingItems;

public interface ShoppingItemsRepository extends JpaRepository<ShoppingItems, Integer>{

	List<ShoppingItems> findByUserId (Integer userId);

	
	
}
