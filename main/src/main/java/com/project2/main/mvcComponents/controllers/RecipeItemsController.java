package com.project2.main.mvcComponents.controllers;

import com.project2.main.mvcComponents.services.RecipeItemsService;

import java.util.ArrayList;
import java.util.List;

import com.project2.main.mvcComponents.models.RecipeItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/recipeItems")
public class RecipeItemsController {
    @Autowired
    private RecipeItemsService recipeItemsService;

    @RequestMapping(value="/{id}")
    public List<RecipeItems> getAllItems(@PathVariable int id){
      return recipeItemsService.getAllItems(id);
    }

    @RequestMapping(method=RequestMethod.POST, value="/update")
    public String updateItems(@RequestBody ArrayList<RecipeItems> items){

      recipeItemsService.modifyRecipe(items);
      return "Good";
    }

    //Needed to insert single items from API.
    public void add (RecipeItems item){
      recipeItemsService.add(item);
    }
}