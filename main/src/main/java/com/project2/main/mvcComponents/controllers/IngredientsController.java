package com.project2.main.mvcComponents.controllers;

import java.util.List;
import java.util.Optional;

import com.project2.main.mvcComponents.models.Ingredients;
import com.project2.main.mvcComponents.services.IngredientsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IngredientsController {
    @Autowired
    private IngredientsService ingredientsService;

    @RequestMapping("/ingredients/all")
    public List<Ingredients> getAllIngredients(){
        return ingredientsService.getAllIngredients();
    }


    @RequestMapping(method=RequestMethod.POST, value="/ingredients")
    public void  addIngredients(@RequestBody Ingredients ingredient){
       
        ingredientsService.addIngredient(ingredient);
    }


    @RequestMapping("/")
    public String welcome(){
        return "Hi";
    }

    public Ingredients getIngredientId(String name){
        return ingredientsService.getIngredientId(name);
    }

    @GetMapping("/ingredients/{id}")
    public Optional<Ingredients> getIngredient(@PathVariable Integer id){
        return ingredientsService.findByIngredientId(id);
    }

    @GetMapping("/ingredients/name/{name}")
    public List<Ingredients> getIngredientByName(@PathVariable String name){
        return ingredientsService.findByIngredientsLike(name);
    }

}