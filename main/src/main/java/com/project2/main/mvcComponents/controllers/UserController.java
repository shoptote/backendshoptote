package com.project2.main.mvcComponents.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project2.main.mvcComponents.models.Users;
import com.project2.main.mvcComponents.services.UserService;



@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService us;
	
	//Can I do this?
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostMapping("/register")
	public ResponseEntity<String> userRegister(@RequestBody Users users) {

		users.setPassword(bCryptPasswordEncoder.encode(users.getPassword()));
		if(!us.userRegistration(users)){
			return new ResponseEntity<String>("Username already in use", HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity<String>("Success", HttpStatus.ACCEPTED);

	}
	
	@GetMapping("/userinfo/{name}")
	public Users getUserInfoByName(@PathVariable String name){
		return us.getUserByUsername(name);
	}

	
}
