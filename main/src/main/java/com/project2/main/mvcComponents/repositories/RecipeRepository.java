package com.project2.main.mvcComponents.repositories;

import com.project2.main.mvcComponents.models.Recipes;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends CrudRepository<Recipes, Integer>{
    
    Recipes findByName(String name);
    Recipes findByApiId(Integer id);
    
}