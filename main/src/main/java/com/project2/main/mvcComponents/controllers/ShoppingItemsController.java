package com.project2.main.mvcComponents.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project2.main.mvcComponents.models.ShoppingItems;
import com.project2.main.mvcComponents.services.ShoppingItemsService;


@RequestMapping("/shoppingitems")
@RestController
public class ShoppingItemsController {
	

	@Autowired
	private ShoppingItemsService ss;
	
	@PostMapping("/add")
	public ShoppingItems addItem(@RequestBody ShoppingItems item) {
		ss.addItem(item);
		return item;
	}
	
	@GetMapping("/all")
	public List<ShoppingItems> getAllItems(){
		return ss.findAll();
	}
	
	@RequestMapping("/userid")
	List<ShoppingItems> findByUserId(@RequestParam(value="userId") Integer userId ){
		return ss.findByUserId(userId);
	}
	
	@DeleteMapping("/deletebyid/{id}")
	public void deleteById(@PathVariable Integer id) {
		ss.deleteById(id);;
	}
	
	@RequestMapping(value="/items/{id}", method=RequestMethod.PUT)
	public void updateItem(@RequestBody ShoppingItems item, @PathVariable Integer id){
		ss.updateItem(id, item);
	}
	
	
}
