package com.project2.main.mvcComponents.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project2.main.mvcComponents.models.ShoppingItems;
import com.project2.main.mvcComponents.repositories.ShoppingItemsRepository;

@Service
public class ShoppingItemsService {
	
	@Autowired
	private ShoppingItemsRepository sr;
	
	public ShoppingItems addItem ( ShoppingItems item) {
		sr.save(item);
		return item;
	}
	
	public List<ShoppingItems> findAll(){
		return sr.findAll();
	}
	 

	public List<ShoppingItems> findByUserId(Integer userId){
		return sr.findByUserId(userId);
	}
	
	public void deleteById( Integer id) {
		sr.deleteById(id);
	}


	public void updateItem(Integer id, ShoppingItems items) {
		sr.save(items);
	}
	
	
}
