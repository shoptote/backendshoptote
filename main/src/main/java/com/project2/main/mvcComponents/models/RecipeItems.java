package com.project2.main.mvcComponents.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



//Let Lombok handle constructors.
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "recipeitems")
public class RecipeItems {

    //Do not serialize, let Jackson handle it.
    //For numerics, user @Min
    //For String, Collections, use @Size
    @Id
    @NotNull(message="Please enter item id")
    @Column(name = "recipeItemId")
    @Min(value=1)
    private int recipeItemId;


    @Column(name = "recipe_id")
    @NotNull(message="Please enter recipe id") 
    @Min(value=1)
    private int recipeId;

    @Column(name = "quantity")
    @NotNull(message="Please enter quantity")
    private double quantity;

    @Column(name = "unit")
    private String unit;



}