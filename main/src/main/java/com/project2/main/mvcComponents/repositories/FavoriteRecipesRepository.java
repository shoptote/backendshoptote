package com.project2.main.mvcComponents.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project2.main.mvcComponents.models.FavoriteRecipes;

public interface FavoriteRecipesRepository extends JpaRepository<FavoriteRecipes, Integer>{

}
