package com.project2.main.mvcComponents.services;

import java.util.ArrayList;
import java.util.List;

import com.project2.main.mvcComponents.models.RecipeItems;
import com.project2.main.mvcComponents.repositories.RecipeItemsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RecipeItemsService {
    @Autowired
    private RecipeItemsRepository recipeItemsRepository;
    private List<RecipeItems> recipeItems = new ArrayList<RecipeItems>();

 

    //Let front end parse it. Need the id for editing.
    public List<RecipeItems> getAllItems(Integer recipeId){
        List<RecipeItems> ing = new ArrayList<RecipeItems>();
       
        recipeItemsRepository.findByRecipeId(recipeId).forEach(ing::add);
        return ing;
    }

    
    //Possible -> Have a form to modify, then call with recipe_id fetched
    //Used for create update.
    public void  modifyRecipe(List<RecipeItems> recItems){
        recipeItemsRepository.saveAll(recItems);
    }

    public void add(RecipeItems toAdd){
        recipeItemsRepository.save(toAdd);
    }
}