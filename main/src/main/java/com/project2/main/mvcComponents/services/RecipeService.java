package com.project2.main.mvcComponents.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.project2.main.mvcComponents.models.Recipes;
import com.project2.main.mvcComponents.repositories.RecipeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecipeService {
    @Autowired
    private RecipeRepository recipeRepository;

    public void addRecipe(Recipes recipe){
        if(recipeRepository.findByName(recipe.getName()) != null){
            return;
        }
        recipeRepository.save(recipe);

    }

    public Recipes getRecipe(String name){
        return recipeRepository.findByName(name);
    }

    public HashMap<String, ArrayList<Recipes>> filterRecipes(ArrayList<Recipes> toFilter){
        HashMap<String, ArrayList<Recipes>> contain = new HashMap<>();
        ArrayList<Recipes> contained = new ArrayList<Recipes>();
        ArrayList<Recipes> noncontained = new ArrayList<Recipes>();
        for(int i = 0; i < toFilter.size(); i++ ){
            Recipes rec= recipeRepository.findByApiId(toFilter.get(i).getApiId());
            if(rec == null){
                noncontained.add(toFilter.get(i));
            }
            else{
                contained.add(rec);
            }
    
        }
        if(noncontained != null){
        recipeRepository.saveAll(noncontained);
        }
        contain.put("contained", contained);
        contain.put("noncontained", noncontained);

        return contain;
    }

    public List<Recipes> getAll(){
        
        List<Recipes> recipes = new ArrayList<>();
        recipeRepository.findAll().forEach(recipes::add);
        return recipes;
    }
}