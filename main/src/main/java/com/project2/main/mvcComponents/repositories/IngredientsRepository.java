package com.project2.main.mvcComponents.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.project2.main.mvcComponents.models.Ingredients;


public interface IngredientsRepository extends CrudRepository<Ingredients, Integer>{
    
    Ingredients findByName(String name);

    @Query("SELECT n FROM Ingredients n WHERE n.name LIKE ?1%")
    List<Ingredients> findByNameLike(String pattern);
    
}