package com.project2.main.mvcComponents.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project2.main.mvcComponents.models.FavoriteRecipes;
import com.project2.main.mvcComponents.models.Ingredients;
import com.project2.main.mvcComponents.models.ShoppingItems;
import com.project2.main.mvcComponents.repositories.FavoriteRecipesRepository;

@Service
public class FavoriteRecipesService {
	
	@Autowired
	private FavoriteRecipesRepository fr;
	
//	private List<FavoriteRecipes> favoriteRecipes = new ArrayList<FavoriteRecipes>();
	
    public FavoriteRecipes addFavorite (FavoriteRecipes recipes){
        fr.save(recipes);
        return recipes;
    }
	
	public List<FavoriteRecipes> findAll(){
		return fr.findAll();
	}
	
	public void deleteById( Integer id) {
		fr.deleteById(id);
	}
}
