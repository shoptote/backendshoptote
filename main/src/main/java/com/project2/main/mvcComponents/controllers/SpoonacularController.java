package com.project2.main.mvcComponents.controllers;

import com.project2.main.mvcComponents.models.Ingredients;
import com.project2.main.mvcComponents.models.RecipeItems;
import com.project2.main.mvcComponents.models.Recipes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@RestController
@RequestMapping("/spoon")
public class SpoonacularController {
    
    private String apiKey=System.getenv("apiKey");




    @Autowired
    private RecipeController recipeController;

    @Autowired
    private IngredientsController ingredientsController;

    @Autowired
    private RecipeItemsController recipeItemsController;
    
    public void getIngredientsOfRecipe(Integer apiRecipeId, Integer currentRecipe){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    
  
        final String uri = "https://api.spoonacular.com/recipes/{id}/ingredientWidget.json?"+ apiKey;
        Map<String, Integer> params = new HashMap<String,Integer>();
        RestTemplate restTemplate = new RestTemplate();
        params.put("id", apiRecipeId);
        //Object result = restTemplate.getForObject(uri, Object.class, params);
        Object result = restTemplate.exchange(uri, HttpMethod.GET,entity, Object.class, params).getBody();
        HashMap<Object, Object> k = (HashMap<Object,Object>)result;
        //HashMap<Object, Object> ingredients = (HashMap<Object,Object>) k.get("ingredients");
        ArrayList<Object> ingredients = (ArrayList<Object>) k.get("ingredients");

        for(int i = 0 ; i < ingredients.size() ; i++){
            HashMap<Object, Object> current = (HashMap<Object, Object>) ingredients.get(i);
            HashMap<Object, Object> amount = (HashMap<Object, Object>) current.get("amount");
            HashMap<Object, Object> us = (HashMap<Object, Object>) amount.get("us");
            Ingredients newIngredient = new Ingredients( 0, (String) (current.get("name")));
            ingredientsController.addIngredients(newIngredient);
            Ingredients fetched = ingredientsController.getIngredientId(newIngredient.getName());
            RecipeItems recipe = new RecipeItems(fetched.getId(), currentRecipe,(double) us.get("value"), (String) us.get("unit"));
            recipeItemsController.add(recipe);
        }
   

    }

    public ArrayList<Recipes> getRecipeByName(String ingredients){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    
        final String uri = "https://api.spoonacular.com/recipes/search?query={ingredients}&"+apiKey;
        Map<String, String> params = new HashMap<String, String>();
        ingredients = ingredients.trim();
        ingredients = ingredients.replace(" ", "%20");
        params.put("ingredients", ingredients);
        System.out.println("---------------------");
        System.out.println("PRERESULTS");
        System.out.println(uri);
        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.exchange(uri, HttpMethod.GET,entity, Object.class, params).getBody();
        //System.out.println("---------------------");
        //System.out.println("PRERESULTS");
        //System.out.println(result);
        HashMap<Object, Object> results = (HashMap<Object,Object>) result;
        ArrayList<Object> recipes = (ArrayList<Object>) results.get("results");

        ArrayList<Recipes> fetched = new ArrayList<Recipes>();
        for(int i = 0 ; i < recipes.size(); i++){
            //System.out.println("---------------------");
            //System.out.println("RESULTS");
          HashMap<Object, Object> k = (HashMap<Object,Object>)recipes.get(i);
          fetched.add(new Recipes(0,(String) k.get("title"),(int)k.get("id")));
            
            }
            HashMap<String, ArrayList<Recipes>> content = recipeController.filter(fetched);
            ArrayList<Recipes> noncontained= content.get("noncontained");
            ArrayList<Recipes> contained= content.get("contained");
            //System.out.println("---------------------");
            //System.out.println("THIS IS UNCONTAINED");
             //System.out.println(noncontained);
            //System.out.println("---------------------");
            //System.out.println("THIS IS CONTAINED");
            //System.out.println(contained);
            //For each uncontained, pass it to save the ingredients.
            //Pass the recipeId.
             for(Recipes list : noncontained){
                 //System.out.println("---------------------");
                 //System.out.println("THIS IS FETCHED");
                 //System.out.println(list.getApiId());
                 getIngredientsOfRecipe(list.getApiId(), list.getRecipeId());
             }
             noncontained.addAll(contained);
             return noncontained;

    }

    public ArrayList<Recipes> getRecipeByIngredients(ArrayList<String> ingredients){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        final String uri = "https://api.spoonacular.com/recipes/findByIngredients?ingredients={ingredients}&"+apiKey;
        String parsedItems = "";
        //System.out.println("---------------------");
         //System.out.println(ingredients.toString());

        ingredients.set(0, ingredients.get(0).replace(" ", ""));
        parsedItems = parsedItems.concat(ingredients.get(0));
        for (int i = 1 ; i < ingredients.size(); i++){
            String token = ",+" + ingredients.get(i).replace(" ", "");
            parsedItems = parsedItems.concat(token);
        }
        //System.out.println("---------------------");
        //System.out.println("PARSED");
        //System.out.println(parsedItems);
        Map<String, String> params = new HashMap<String, String>();
        params.put("ingredients", parsedItems);

        RestTemplate restTemplate = new RestTemplate();
        Object[] result = restTemplate.exchange(uri, HttpMethod.GET,entity, Object[].class, params).getBody();
        
        
        ArrayList<Recipes> fetched = new ArrayList<Recipes>();
        for(int i = 0 ; i < result.length; i++){
            //System.out.println("---------------------");
            //System.out.println("RESULTS");
          HashMap<Object, Object> k = (HashMap<Object,Object>)result[i];
          fetched.add(new Recipes(0,(String) k.get("title"),(int)k.get("id")));
            
            }
            //System.out.println("---------------------");
            //Inside of this call, divide which we currently have stored,
            //To those we don't currently have.
            //If we don't have them, save them inside.
            //System.out.println(fetched);
            HashMap<String, ArrayList<Recipes>> content = recipeController.filter(fetched);
           ArrayList<Recipes> noncontained= content.get("noncontained");
           ArrayList<Recipes> contained= content.get("contained");
           //System.out.println("---------------------");
           //System.out.println("THIS IS UNCONTAINED");
            //System.out.println(noncontained);
           //System.out.println("---------------------");
           //System.out.println("THIS IS CONTAINED");
           //System.out.println(contained);
           //For each uncontained, pass it to save the ingredients.
           //Pass the recipeId.
            for(Recipes list : noncontained){
                //System.out.println("---------------------");
                //System.out.println("THIS IS FETCHED");
                //System.out.println(list.getApiId());
                getIngredientsOfRecipe(list.getApiId(), list.getRecipeId());
            }
            noncontained.addAll(contained);
            return noncontained;
    }


  
    @RequestMapping(method=RequestMethod.POST, value="/ingredients")
    public List<Recipes> toCallByIngredient(@RequestBody List<String> ingredients){
        //System.out.println(ingredients);
       ArrayList<Recipes> done = getRecipeByIngredients((ArrayList<String>) ingredients);
        //System.out.println("THIS IS DONE");
        //System.out.println(done);
        return done;
    }

    @RequestMapping(method=RequestMethod.POST, value="/name")
    public List<Recipes> toCallByName(@RequestBody String query){
       ArrayList<Recipes> done = getRecipeByName(query);
        //System.out.println("THIS IS DONE");
        //System.out.println(done);
        return done;
    }

        
	@GetMapping("/recipes/bynutrients")
	private Object getRecipesByIngredient(@RequestParam (value="minCarbs") String minCarbs, @RequestParam (value="minFat") String minFat) {
		try {
			String url = "https://api.spoonacular.com/recipes/findByNutrients?minCarbs={minCarbs}&minFat={minFat}&"+apiKey;
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			Object response = restTemplate.exchange(url, HttpMethod.GET,entity,Object.class,minCarbs,minFat).getBody();
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}




	
	@GetMapping("/recipes")
	private Object getRecipes(@RequestParam (value="query") String query, @RequestParam (value="cuisine") String cuisine) {
		try {
			String url = "https://api.spoonacular.com/recipes/search?query={query}&number=5&"+apiKey+"&cuisine={cuisine}";
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			Object response = restTemplate.exchange(url, HttpMethod.GET,entity,Object.class,query,cuisine).getBody();
			 
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
    }
    
}