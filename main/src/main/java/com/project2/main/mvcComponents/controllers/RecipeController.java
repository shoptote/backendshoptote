package com.project2.main.mvcComponents.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.websocket.server.PathParam;

import com.project2.main.mvcComponents.models.Recipes;
import com.project2.main.mvcComponents.services.RecipeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/recipes")
public class RecipeController {
    @Autowired
    private RecipeService recipeService;

    public void addRecipe(int recipeId, String name){
        Recipes recipe = new Recipes(recipeId, name, 0);
        recipeService.addRecipe(recipe);
    }

    @PostMapping("/add")
    public void addRecipe(@RequestBody Recipes recipe){
        recipeService.addRecipe(recipe);
    }

    @GetMapping("/{name}")
    public Recipes getRecipe(@PathVariable String name){
        Recipes recipe = recipeService.getRecipe(name);
        return recipe;
    }

    @GetMapping("/all")
    public List<Recipes> getAll(){
        return recipeService.getAll();
    }


    public HashMap<String, ArrayList<Recipes>> filter(ArrayList<Recipes> toFilter){

        return recipeService.filterRecipes(toFilter);
    }
}