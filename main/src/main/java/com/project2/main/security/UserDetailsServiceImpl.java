package com.project2.main.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

import com.project2.main.mvcComponents.repositories.UsersRepository;
import com.project2.main.mvcComponents.models.Users;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UsersRepository  userRepository;

    public UserDetailsServiceImpl(UsersRepository userRepository){
        this.userRepository=userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws  UsernameNotFoundException{
         Users user= userRepository.findByUsername(username);
         if(user == null){
             throw new UsernameNotFoundException(username);
         }
         return new User(user.getUsername(), user.getPassword(), emptyList());
    }
}