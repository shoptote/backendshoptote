package com.project2.main.security;

//In case of asymmetric
//import java.security.Key;

public class SecurityConstants {
    //public static final Key SECRET = new
    //Throw away hash.
    public static final String SECRET = "86951dc765bef95f9474669cd18df7705d99ae47ea3e76a2ca4c22f71656f42ea66e3acdc898c93f475009fa599d0bb83bd5365f36a9cb92c570708f8de5fae8";
    public static final long EXPIRATION_TIME = 864_000_000; //10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/user/register";
    
}