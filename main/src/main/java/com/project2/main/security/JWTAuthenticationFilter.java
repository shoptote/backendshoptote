package com.project2.main.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project2.main.mvcComponents.models.Users;

import java.io.IOException;
import java.security.Key;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.core.userdetails.User;

import static com.project2.main.security.SecurityConstants.EXPIRATION_TIME;
import static com.project2.main.security.SecurityConstants.HEADER_STRING;
import static com.project2.main.security.SecurityConstants.SECRET;
import static com.project2.main.security.SecurityConstants.TOKEN_PREFIX;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
    private AuthenticationManager authenticationManager;
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager){
        this.authenticationManager=authenticationManager;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException{
        try{
          
            Users creds = new ObjectMapper().readValue(req.getInputStream(), Users.class);
            
            return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(creds.getUsername(),creds.getPassword(),new ArrayList<>()));
               
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, 
    HttpServletResponse res, 
    FilterChain chain, 
    Authentication auth) throws IOException, ServletException{
      /*  String token = Jwts.create()
        .withSubject((( User ) auth.getPrincipal()).getUsername())
        .withExpiresAt(new Date(System.currentTimeMillis()+ EXPIRATION_TIME))
        ,sign(SECRET);
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);*/
      
        String token = Jwts.builder()
        .setSubject(((User) auth.getPrincipal()).getUsername())
        .setExpiration(new Date(System.currentTimeMillis()+ EXPIRATION_TIME))
        .signWith(Keys.hmacShaKeyFor(SECRET.getBytes()))
        .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token );
        res.setHeader("Access-Control-Expose-Headers", "Authorization");
    }
}